from datetime import datetime, timedelta
from quart import Blueprint, request, jsonify
from database import tokens, hoard

fetch_bpt = Blueprint("fetch", __name__)


@fetch_bpt.route("/<string:date>", methods=["POST"])
async def _get_application_date(date):
    data = None
    if request.is_json:
        data = await request.json
    else:
        data = await request.form
    if (
        "token" not in data
        or not data["token"]
        or not tokens.find_one({"token": data["token"]})
    ):
        return jsonify({"status": "failure", "data": "Unauthorized"}), 401
    ondate = datetime.strptime(date, "%Y-%m-%d")
    next_day = ondate + timedelta(days=1)
    logs = None
    try:
        logs = list(hoard.find({"time": {"$gte": ondate, "$lt": next_day}}))
    except Exception as e:
        return jsonify({"status": "failure", "data": str(e)}), 400
    if not logs:
        return (
            jsonify({"status": "failure", "data": f"No logs for {date}"}),
            404,
        )
    for log in logs:
        log["_id"] = str(log["_id"])
        log["time"] = log["time"].strftime("%Y-%m-%dT%H:%M:%S.%f")
    return jsonify({"status": "success", "data": logs}), 200
