from quart import Blueprint, request, jsonify
from datetime import datetime
from database import hoard

log_bpt = Blueprint("log", __name__)


@log_bpt.route("/", methods=["POST"])
def _log():
    async def _log():
        data = None
        if request.is_json:
            data = await request.json
        else:
            data = await request.form
        if (
            not data["time"]
            and not data["hostname"]
            and not data["application"]
            and not data["level"]
            and not data["message"]
        ):
            return (
                jsonify(
                    {"status": "failure", "reason": "Missing required info"}
                ),
                400,
            )
        log = {
            "time": datetime.strptime(data["time"], "%Y-%m-%dT%H:%M:%S.%f"),
            "hostname": data["hostname"],
            "application": data["application"],
            "level": data["level"].lower(),
            "message": data["message"],
            "extra": data["extra"] if data["extra"] else {},
        }
        id = ""
        try:
            id = hoard.insert_one(log).inserted_id
        except Exception as e:
            return jsonify({"status": "failure", "data": str(e)}), 400
        return jsonify({"status": "success", "data": str(id)}), 201
