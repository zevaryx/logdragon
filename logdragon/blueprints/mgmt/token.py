from quart import Blueprint, request, jsonify
import ulid
from database import tokens

token_bpt = Blueprint("token", __name__)


@token_bpt.route("/create", methods=["POST"])
async def _create_token():
    data = None
    if request.is_json:
        data = await request.json
    else:
        data = await request.form
    if (
        "token" not in data
        or not data["token"]
        or not tokens.find_one({"token": data["token"]})
    ):
        return jsonify({"status": "failure", "data": "Unauthorized"}), 401
    if not tokens.find_one({"token": data["token"], "admin": True}):
        return jsonify({"status": "failure", "data": "Forbidden"}), 403

    if "email" not in data:
        return jsonify({"status": "failure", "data": "Email required"}), 400

    token = ulid.new().str
    try:
        tokens.insert_one(
            {"token": token, "admin": False, "email": data["email"]}
        )
    except Exception as e:
        return jsonify({"status": "failure", "data": str(e)}), 400

    return jsonify({"status": "success", "data": token}), 201


@token_bpt.route("/revoke", methods=["POST"])
async def _revoke_token():
    data = None
    if request.is_json:
        data = await request.json
    else:
        data = await request.form
    if (
        "token" not in data
        or not data["token"]
        or not tokens.find_one({"token": data["token"]})
    ):
        return jsonify({"status": "failure", "data": "Unauthorized"}), 401
    if not tokens.find_one({"token": data["token"], "admin": True}):
        return jsonify({"status": "failure", "data": "Forbidden"}), 403

    if "to_revoke" not in data:
        return (
            jsonify({"status": "failure", "data": "to_revoke required"}),
            400,
        )

    try:
        tokens.delete_one({"token": data["to_revoke"]})
    except Exception as e:
        return jsonify({"status": "failure", "data": str(e)}), 400

    return jsonify({"status": "success", "data": ""}), 200


@token_bpt.route("/elevate", methods=["POST"])
async def _elevate_token():
    data = None
    if request.is_json:
        data = await request.json
    else:
        data = await request.form
    if (
        "token" not in data
        or not data["token"]
        or not tokens.find_one({"token": data["token"]})
    ):
        return jsonify({"status": "failure", "data": "Unauthorized"}), 401
    if not tokens.find_one({"token": data["token"], "admin": True}):
        return jsonify({"status": "failure", "data": "Forbidden"}), 403

    if "to_elevate" not in data:
        return (
            jsonify({"status": "failure", "data": "to_elevate required"}),
            400,
        )

    try:
        tokens.update_one(
            {"token": data["to_elevate"]}, {"$set": {"admin": True}}
        )
    except Exception as e:
        return jsonify({"status": "failure", "data": str(e)}), 400

    return jsonify({"status": "success", "data": ""}), 200
