from datetime import datetime, date, timedelta
from quart import Blueprint, request, jsonify
from database import tokens, hoard

app_bpt = Blueprint("app", __name__)


@app_bpt.route("/<string:name>", methods=["POST"])
async def _get_application(name):
    data = None
    if request.is_json:
        data = await request.json
    else:
        data = await request.form
    if (
        "token" not in data
        or not data["token"]
        or not tokens.find_one({"token": data["token"]})
    ):
        return jsonify({"status": "failure", "data": "Unauthorized"}), 401
    logs = None
    midnight = datetime.combine(date=date.today(), time=datetime.min.time())
    try:
        logs = list(
            hoard.find({"application": name, "time": {"$gte": midnight}})
        )
    except Exception as e:
        return jsonify({"status": "failure", "data": str(e)}), 400
    if not logs:
        return (
            jsonify(
                {
                    "status": "failure",
                    "data": f"Application '{name}' not found",
                }
            ),
            404,
        )
    for log in logs:
        log["_id"] = str(log["_id"])
        log["time"] = log["time"].strftime("%Y-%m-%dT%H:%M:%S.%f")
    return jsonify({"status": "success", "data": logs}), 200


@app_bpt.route("/<string:name>/all", methods=["POST"])
async def _get_application_all(name):
    data = None
    if request.is_json:
        data = await request.json
    else:
        data = await request.form
    if (
        "token" not in data
        or not data["token"]
        or not tokens.find_one({"token": data["token"]})
    ):
        return jsonify({"status": "failure", "data": "Unauthorized"}), 401
    logs = None
    try:
        logs = list(hoard.find({"application": name}))
    except Exception as e:
        return jsonify({"status": "failure", "data": str(e)}), 400
    if not logs:
        return (
            jsonify(
                {
                    "status": "failure",
                    "data": f"Application '{name}' not found",
                }
            ),
            404,
        )
    for log in logs:
        log["_id"] = str(log["_id"])
        log["time"] = log["time"].strftime("%Y-%m-%dT%H:%M:%S.%f")
    return jsonify({"status": "success", "data": logs}), 200


@app_bpt.route("/<string:name>/date", methods=["POST"])
async def _get_application_date(name):
    data = None
    if request.is_json:
        data = await request.json
    else:
        data = await request.form
    if (
        "token" not in data
        or not data["token"]
        or not tokens.find_one({"token": data["token"]})
    ):
        return jsonify({"status": "failure", "data": "Unauthorized"}), 401
    ondate = None
    if "date" not in data:
        ondate = datetime.combine(date=date.today(), time=datetime.min.time())
    else:
        ondate = datetime.strptime(data["date"], "%Y-%m-%d")
    next_day = ondate + timedelta(days=1)
    logs = None
    try:
        logs = list(
            hoard.find(
                {
                    "application": name,
                    "time": {"$gte": ondate, "$lt": next_day},
                }
            )
        )
    except Exception as e:
        return jsonify({"status": "failure", "data": str(e)}), 400
    if not logs:
        return (
            jsonify(
                {
                    "status": "failure",
                    "data": f"Application '{name}' not found on date {data['date']}",
                }
            ),
            404,
        )
    for log in logs:
        log["_id"] = str(log["_id"])
        log["time"] = log["time"].strftime("%Y-%m-%dT%H:%M:%S.%f")
    return jsonify({"status": "success", "data": logs}), 200


@app_bpt.route("/<string:name>/range", methods=["POST"])
async def _get_application_range(name):
    data = None
    if request.is_json:
        data = await request.json
    else:
        data = await request.form
    if (
        "token" not in data
        or not data["token"]
        or not tokens.find_one({"token": data["token"]})
    ):
        return jsonify({"status": "failure", "data": "Unauthorized"}), 401
    start = None
    if "start" not in data:
        start = datetime.combine(date=date.today(), time=datetime.min.time())
    else:
        start = datetime.strptime(data["start"], "%Y-%m-%d")
    end = None
    if "end" not in data:
        end = datetime.now()
    else:
        end = datetime.strptime(data["end"], "%Y-%m-%d")
    logs = None
    try:
        logs = list(
            hoard.find(
                {"application": name, "time": {"$gte": start, "$lt": end}}
            )
        )
    except Exception as e:
        return jsonify({"status": "failure", "data": str(e)}), 400
    if not logs:
        return (
            jsonify(
                {
                    "status": "failure",
                    "data": f"Application '{name}' not found in range {data['start']} to {data['end']}",
                }
            ),
            404,
        )
    for log in logs:
        log["_id"] = str(log["_id"])
        log["time"] = log["time"].strftime("%Y-%m-%dT%H:%M:%S.%f")
    return jsonify({"status": "success", "data": logs}), 200
