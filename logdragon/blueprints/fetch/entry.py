from quart import Blueprint, jsonify, request
from database import tokens, hoard
from bson import ObjectId

entry_bpt = Blueprint("entry", __name__)

# Requires token
@entry_bpt.route("/<string:id>", methods=["POST"])
async def _get_entry(id):
    data = None
    if request.is_json:
        data = await request.json
    else:
        data = await request.form
    if (
        "token" not in data
        or not data["token"]
        or not tokens.find_one({"token": data["token"]})
    ):
        return jsonify({"status": "failure", "data": "Unauthorized"}), 401
    log = None
    try:
        log = hoard.find_one({"_id": ObjectId(id)})
    except Exception as e:
        return jsonify({"status": "failure", "data": str(e)}), 400
    if not log:
        return jsonify({"status": "failure", "data": "Entry not found"}), 404
    log["_id"] = str(log["_id"])
    log["time"] = log["time"].strftime("%Y-%m-%dT%H:%M:%S.%f")
    return jsonify({"status": "success", "data": log}), 200
