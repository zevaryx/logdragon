from database import tokens
import ulid

from quart import Quart

from blueprints import log
from blueprints.mgmt import token as mtoken
from blueprints.fetch import app as fapp, entry as fentry
from blueprints import ffetch as fetch_root

app = Quart(__name__)


def init_api():
    # Database
    # database.init()
    # /log endpoint
    app.register_blueprint(log.log_bpt, url_prefix="/api/v1/log")
    # /mgmt endpoint
    app.register_blueprint(mtoken.token_bpt, url_prefix="/api/v1/mgmt/token")
    # /fetch endpoint
    app.register_blueprint(fetch_root.fetch_bpt, url_prefix="/api/v1/fetch")
    app.register_blueprint(fapp.app_bpt, url_prefix="/api/v1/fetch/app")
    app.register_blueprint(fentry.entry_bpt, url_prefix="/api/v1/fetch/entry")


# Make sure at least one token exists
def verify_tokens():
    admin_token = tokens.find_one({"email": "admin@localhost"})
    if not admin_token:
        print("WARNING: Admin token not found. Creating...")
        token = ulid.new().str
        email = "admin@localhost"
        admin = True
        tokens.insert_one({"email": email, "token": token, "admin": admin})
        admin_token = tokens.find_one({"email": "admin@localhost"})
    print("Admin token:", admin_token["token"])


if __name__ == "__main__":
    init_api()
    verify_tokens()
    for url in app.url_map.iter_rules():
        print("https://localhost:5700" + str(url))
    app.run(debug=True, port=5700)
