from pymongo import MongoClient
from secrets import mongo

client = MongoClient(**mongo)
db = client.logdragon
hoard = db.hoard
tokens = db.tokens
