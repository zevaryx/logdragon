# Admin CLI interface
import requests
from datetime import datetime, date
from readchar import readkey

CTRLC = "\x03"

host = "localhost"
port = 5700
api = "/api/v1"
token = None

url = f"http://{host}:{port}{api}"


def build_url(host, port, api):
    return f"http://{host}:{port}{api}"


def print_logs(logs):
    for log in logs:
        print(
            f"""
Application: {log['name']}
   Hostname: {log['hostname']}
       Time: {log['time']}
      Level: {log['level']}
    Message: {log['message']}"""
        )
        if "extra" in log:
            print("      Extra:")
            for k in log["extra"]:
                print(f"             {k}: {log['extra'][k]}")


def make_request(endpoint, data=None):
    if not data:
        data = {}
    if "token" not in data:
        data["token"] = token
    res = requests.post(endpoint, json=data)
    if 200 <= res.status_code <= 299 or 400 <= res.status_code <= 499:
        return res.json()
    raise requests.RequestException()


def get_application(name):
    data = make_request(f"{url}/fetch/app/{name}")
    if data["status"] == "failure":
        print("Failed to get application.\nReason: ", data["data"])
    else:
        print_logs(data["data"])


def get_application_date(name, adate):
    data = make_request(f"{url}/fetch/app/{name}/date", {"date": adate})
    if data["status"] == "failure":
        print(f"Failed to get application on {date}.\nReason: ", data["data"])
    else:
        print_logs(data["data"])


def get_application_range(name, start, end):
    rdata = {"start": start, "end": end}
    data = make_request(f"{url}/fetch/app/{name}/range", rdata)
    if data["status"] == "failure":
        print(
            f"Failed to get application in range {start} - {end}.\nReason: ",
            data["data"],
        )
    else:
        print_logs(data["data"])


main_menu = """
[G]et Application
[L]og Test
[Q]uit
"""

get_application_menu = """
[A]pplication
Application [D]ate
Application [R]ange
[B]ack
"""


def get_application_prompt():
    prompt = "app >"
    getting = True
    while getting:
        print(get_application_menu)
        print(prompt, end=" ", flush=True)
        k = readkey()
        print(k)
        if k in "Bb":
            getting = False
        elif k == CTRLC:
            print("KeyboardInterrupt")
            exit(0)
        elif k in "Aa":
            aname = input("Application name: ")
            get_application(aname)
        elif k in "Dd":
            aname = input("Application name: ")
            adate = input(" Date (mm-dd-yy): ")
            get_application_date(aname, adate)
        elif k in "Rr":
            aname = input("Application name: ")
            astart = input("Start (mm-dd-yy): ")
            aend = input("  End (mm-dd-yy): ")
            get_application_range(aname, astart, aend)
        else:
            print("Unknown option", k)


def main():
    prompt = ">"
    running = True
    while running:
        print(main_menu)
        print(prompt, end=" ", flush=True)
        k = readkey()
        print(k)
        if k == CTRLC:
            print("KeyboardInterrupt")
            exit(0)
        elif k in "Qq":
            print("Exiting...")
            running = False
            break
        elif k in "Gg":
            get_application_prompt()
        else:
            print("Unknown option", k)


if __name__ == "__main__":
    if not token:
        token = input("Please enter token: ")
    main()
