# LogDragon

LogDragon is a RESTful log aggregator written in Python with a MongoDB database.

## Log Format

LogDragon uses a custom log format for simplicity. The format contains:

* time
* hostname/IP
* application
* level
* message
* extra (optional)

JSON example:

```json
{
  "time": "%Y-%m-%dT%H:%M:%S.%f",
  "hostname": "localhost",
  "application": "LogDragon",
  "level": "debug",
  "message": "README Message",
  "extra": {
    "extra": "info here"
  }
}
```

All requests are made via POST HTTP requests.

## Return format

LogDragon will return an `application/json` message on submission of any log, in the following format:

```json
{
  "status": "success/failure",
  "data": "reason for failure, expected data for success"
}
```


## Example Usage

Python:
```py
import requests
from datetime import datetime
from socket import gethostname

url = "https://localhost:5700/api/v1/log"

log = {
  "time": datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f"),
  "hostname": gethostname(),
  "application": "LogDragon",
  "level": "debug",
  "message": "README Message",
  "extra": {
    "user": "zevaryx"
  }
}
status = requests.post(url, json=log)
```

## Installation

LogDragon requires a [MongoDB](https://www.mongodb.com/) installation.

Once MongoDB is installed and configured, clone this repository and copy `secrets.example.py` to `secrets.py`, and update the file with the correct credentials to connect to your MongoDB instance.

Run `pip install requirements.txt` in the main directory to install the required pip packages.

Run `python logdragon.py` to start the server.

## API

LogDragon has a full token-based API for log querying. Below are the endpoints:

```
API root:
/api/v1

  Unauthenticated:
  /log

  Requires token:
  /get
    /entry/<string:id>
    /application/<string:name>
      /all
      /date
      /range

  Admin only:
  /token
    /create
    /revoke
    /elevate
```

## API Guide

**Bold** = required

### Unauthenticated endpoints

#### `/log`

Logs a new message

Params:

* **`time`**
  * Time of the log message
  * Example: `2020-02-26T09:53:48.372000`
* **`hostname`**
  * Hostname of system log was generated on
  * Example: `host.example.com`
* **`application`**
  * Application name
  * Example: `LogDragon`
* **`level`**
  * Log level
  * Example: `debug`
* **`message`**
  * Log message
  * Example: `Log message`
* `extra`
  * Dictionary of extra data
  * Example: `{"user": "zevaryx"}`


### Token endpoints

#### `/entry`

All commands that fetch information for entries.

Global Params:

* **`token`**
  * Token of requestor
  * All tokens are [ULIDs](https://github.com/ulid/spec)
  * Example: `01ARZ3NDEKTSV4RRFFQ69G5FAV`

##### `/<string:id>`

Looks up a log entry with BsonId `id`

Params: None

#### `/application`

All commands that fetch information for applications.

Global Params:

* **`token`**
  * Token of requestor
  * All tokens are [ULIDs](https://github.com/ulid/spec)
  * Example: `01ARZ3NDEKTSV4RRFFQ69G5FAV`

##### `/<string:name>`

Looks up all logs for application `name` from the current day

Params: None

##### `/<string:name>/all`

Gets all log entries from the beginning of time for application `name`

Params: None

##### `/<string:name>/date`

Gets all log entries on specified date for application `name`

Params:

* `date`
  * Date for lookup
  * Default: today's date
  * Example: `2020-2-26`

##### `/<string:name>/range`

Gets all log entries in specified date range for application `name`

Params:

* `start`
  * Date for lookup start
  * Default: Today's date
  * Example: `2020-2-26`
* `end`
  * Date for lookup end
  * Default: Time of request (`datetime.now()`)
  * Example: `2020-2-26`

### Admin endpoints

#### `/token`

All commands that create, modify, and destroy tokens.

**Token must be an admin token**

Global Params:

* **`token`**
  * Token of requestor
  * All tokens are [ULIDs](https://github.com/ulid/spec)
  * Example: `01ARZ3NDEKTSV4RRFFQ69G5FAV`

##### `/create`

Creates a new token for specified email

Params:

* **`email`**
  * Token owner's email
  * Example: `email@example.com`

##### `/revoke`

Revokes/deletes specified token

Params:

* **`to_revoke`**
  * Token to revoke
  * Example: `01ARZ3NDEKTSV4RRFFQ69G5FAV`

##### `/elevate`

Elevates token permissions to admin

Params:

* **`to_elevate`**
  * Token to elevate
  * Example: `01ARZ3NDEKTSV4RRFFQ69G5FAV`
